CROSS = 
CC = $(CROSS)clang
STRIP = $(CROSS)strip
CFLAGS =
LDFLAGS =
GIT_VERSION = $(shell ./gitver.sh)
GLFW_CFLAGS = $(shell PKG_CONFIG_PATH=./deps/lib/pkgconfig pkg-config glfw3 --cflags)
GLFW_LDFLAGS = $(shell PKG_CONFIG_PATH=./deps/lib/pkgconfig pkg-config glfw3 --libs)
CFLAGS_ = -std=c11 -pedantic -DGIT_VERSION="\"$(GIT_VERSION)\"" -Ideps/include
LDFLAGS_ = -Ldeps/lib -lm -lgps
OBJDIR = obj
OUT = radtrak

ifneq ($(strip $(shell $(CC) -v 2>&1 | grep "^Target" | grep -i "mingw")),)
	LDFLAGS_ += -lopengl32
else
	LDFLAGS_ += -lpthread -lGL -lGLEW
endif

CFLAGS_ += $(CFLAGS) $(GLFW_CFLAGS)
LDFLAGS_ += $(LDFLAGS) $(GLFW_LDFLAGS)

.PHONY: default release debug profile strip clean
default: debug

release: CFLAGS_ += -O2 -g3
release: $(OUT)

debug: CFLAGS_ += -O0 -g3
debug: $(OUT)

profile: CFLAGS_ += -O0 -g3 -pg
profile: LDFLAGS_ += -pg
profile: $(OUT)

strip: release
	@$(STRIP) $(OUT)

SRC = $(wildcard src/*.c)
HDR = $(wildcard src/*.h)
OBJ = $(patsubst src/%.c,$(OBJDIR)/%.o,$(SRC))

$(OBJDIR)/%.o: src/%.c $(HDR)
	@mkdir -p $(OBJDIR)
	$(CC) -o $@ $(CFLAGS_) -c $<

$(OUT): $(OBJ)
	$(CC) -o $(OUT) $(OBJ) $(LDFLAGS_)

clean:
	@rm -rf $(OUT) $(OBJDIR)

clean-deps:
	@rm -rf deps/lib deps/include deps/src/glfw/build

