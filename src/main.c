#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <signal.h>
#include <termios.h>

#define DAEMON 1

static char *homedir = NULL;



int main ( int argc, char **argv ) 
{

    if ( !homedir )
    {
        char *home = getenv ("HOME");
        homedir = malloc (strlen ("/.pkgd") + strlen (home) + 1);
        sprintf (homedir, "%s/.pkgd", home);
    }

    struct stat tmpstat;
    if (stat (homedir, &tmpstat))
    {
        if ( !mkdir (homedir, S_IRUSR | S_IWUSR | S_IXUSR) )
        {
            printf ("Created home directory: %s", homedir);
        }
        else
        {
            fprintf (stderr, "Couldn't not create directory: %s", homedir);
        }
    }

    if (chdir (homedir) < 0)
    {
        fprintf (stderr, "chdir failed\n");
        return 1;
    }

    FILE* tty_port = fopen( "/dev/ttyUSB0", "rw");
    FILE* formatted_file = fopen("fouput", "w");
    FILE* raw_file = fopen("rawouput", "w");
    struct termios tty;
    memset (&tty, 0, sizeof(tty));
    double cpm, uSv, uSvd;



    if ( DAEMON == 1 ) 
    {
        pid_t pid;
        FILE* pidfile = fopen("pktgd.pid", "w");

        if ( !pidfile )
        {
            fprintf (stderr, "failed to open pidfile\n");
            return 1;
        }

        pid = fork ();

        if ( pid > 0 )
        {
            printf ("Forked to backgorund (pid %i)\n", (int)pid);
            fprintf (pidfile, "%i\n", (int)pid);
            fclose (pidfile);
            return 0;
        }
        else if ( pid == 0 )
        {
            freopen ("/dev/null", "w", stdout );
            freopen ("/dev/null", "w", stderr );
            fclose (pidfile);
        }
        else
        {
            fprintf (stderr, "Failed to fork\n");
            return 1;
        }
    }

    if ( !tty_port )
    {
        fprintf(stderr, "Couldn't open /dev/ttyUSB0\n");
        return 1;
    }

    if ( !formatted_file )
    {
        fprintf(stderr, "Couldn't open the formatted output\n");
        return 1;
    }

    if ( !raw_file )
    {
        fprintf(stderr, "Couldn't open the raw output\n");
    }

    if ( tcgetattr ( tty_port->_fileno, &tty ) != 0 ) 
    {
        fprintf(stderr, "error from tcgetattr\n");
        return 1;
    }




    cfsetospeed ( &tty, 9800 );
    cfsetispeed ( &tty, 9800 );


    tty.c_cflag = ( tty.c_cflag & ~CSIZE ) | CS8;
    tty.c_iflag &= ~IGNBRK;
    tty.c_lflag = 0;
    tty.c_oflag = 0;
    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 5;

    tty.c_iflag &= ~(IXON | IXOFF | IXANY );

    tty.c_cflag |= (CLOCAL | CREAD);

    tty.c_cflag &= ~(PARENB| PARODD);
    tty.c_cflag |= 0; //parity
    tty.c_cflag &= ~CSTOPB;
    //tty.c_cflag &= ~CRTSCTS;

    if ( tcsetattr (tty_port->_fileno, TCSANOW, &tty ) != 0 )
    {
        printf ("error from tcsetattr");
    }




    while ( fscanf( tty_port, "%lf,%lf,%lf,", &cpm, &uSv, &uSvd ) != EOF )
    {
        if ( cpm != 0 || uSv != 0 || uSvd != 0 )
        {
            rewind(formatted_file);
            rewind(raw_file);
            printf("Current cpm %f, uSv %f, uSvd %f\n", cpm, uSv, uSvd);
            fprintf(formatted_file, "Current cpm %f, uSv %f, uSvd %f\n", cpm, uSv, uSvd);
            fprintf(raw_file, "%f,%f,%f", cpm, uSv, uSvd);
            fflush(formatted_file);
            fflush(raw_file);
        }
    }
    
    fclose(formatted_file);
    fclose(raw_file);
    fclose(tty_port);
    return 0;
}
