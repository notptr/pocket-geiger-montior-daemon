#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>

#include "gps.h"
#include "gpsdclient.h"

static char *homedir = NULL;
static char *filename = NULL;
static char *inputfile = "rawouput";
static struct gps_data_t gps_data_mine;

int main( int argc, char **argv )
{
    if ( !homedir )
    {
        char *home = getenv ("HOME");
        homedir = malloc (strlen ("/.pkgd") + strlen (home) + 1 );
        sprintf (homedir, "%s/.pkgd", home);
    }

    if ( !filename )
    {
        time_t rawtime;
        struct tm *timeinfo;
        time ( &rawtime );
        timeinfo = localtime (&rawtime);

        char buffer[80];

        strftime(buffer, 80, "%F_%H:%M:%S", timeinfo);
        filename = malloc (strlen(".csv") + strlen (buffer) + 1);

        sprintf (filename, "%s.csv", buffer);
    }

    if ( chdir ( homedir) < 0 )
    {
        fprintf (stderr, "Chdir failed\n");
        return 1;
    }

    int ret = 0;

    ret = gps_open ("localhost", DEFAULT_GPSD_PORT, &gps_data_mine);

    if ( ret < 0 )
    {
        fprintf (stderr, "Couldn't open the gpsd\n");
        return 1;
    }
    

    gps_stream ( &gps_data_mine, WATCH_ENABLE | WATCH_JSON, NULL );

    FILE* csv_file = fopen(filename, "w");
    FILE* input_file = fopen(inputfile, "r");

    if ( !csv_file )
    {
        fprintf (stderr, "couldn't open output file\n");
        return 1;
    }

    if ( !input_file )
    {
        fprintf (stderr, "Couldn't open input file\n");
        return 1;
    }

    int save_info = 0;
    int stop = 0;

    while (stop == 0)
    {
        /* High DPI displays */
        float cpm, uSv, uSvd;
        char lat_buf[256];
        char lon_buf[256];
        char time_buf[256];

        while ( fscanf( input_file, "%f,%f,%f", &cpm, &uSv, &uSvd ) != EOF )
        {
        }

        if ( gps_read ( &gps_data_mine ) == -1 ) 
        {
            fprintf(stdout, "Socket error\n");
        }
        else
        {
            if ( gps_data_mine.fix.mode >= MODE_2D && isnan(gps_data_mine.fix.latitude) == 0 ) 
            {
                sprintf ( lat_buf, "%s %c", deg_to_str ( deg_dd, fabs (gps_data_mine.fix.latitude)), (gps_data_mine.fix.latitude < 0)? 'S' : 'N');
            }
            else 
            {
                sprintf ( lat_buf, "N/a");
            }

            if ( gps_data_mine.fix.mode >= MODE_2D && isnan(gps_data_mine.fix.longitude) == 0 ) 
            {
                sprintf ( lon_buf, "%s %c", deg_to_str ( deg_dd, fabs (gps_data_mine.fix.longitude)), (gps_data_mine.fix.longitude < 0)? 'W' : 'E');
            }
            else
            {
                sprintf ( lon_buf, "N/a" );
            }

            if ( isnan ( gps_data_mine.fix.time ) == 0 ) 
            {
                unix_to_iso8601 ( gps_data_mine.fix.time, time_buf, sizeof(time_buf) );
            }
            else
            {
                sprintf ( time_buf, "N/a" );
            }

        }

        printf ( "CPM: %f, uSv: %f, uSvd: %f, Lat: %s, Lon: %s\n", cpm, uSv, uSvd, lat_buf, lon_buf );

        if ( save_info == 1 )
        {
            fprintf(csv_file, "%s,%s,%s,%f,%f,%f\n", time_buf, lat_buf, lon_buf, cpm, uSv, uSvd);
        }
    }
    fclose(csv_file);
    fclose(input_file);
    gps_stream(&gps_data_mine, WATCH_DISABLE, NULL);
    gps_close(&gps_data_mine);
    return 0;
}

